// Capsule du lieu
import React from 'react';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import './Main.css';

function Location({ location }) {


  return <p id="location">{location}</p>;
}

Location.propTypes = {
  location: PropTypes.string.isRequired,
};

export default Location;
