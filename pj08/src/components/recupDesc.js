// Capsule de récuperation description
import React from 'react';
import { useParams } from 'react-router-dom';
import data from './datas/logements.json';


function ContDescription() {
  const { id } = useParams();

  const logement = data.find(item => item.id === id);
  return (
      <p id="Desc">{logement.description}</p>
  );
}
export default ContDescription;
