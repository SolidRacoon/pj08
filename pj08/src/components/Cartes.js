// CapsulCreation cartes
import React from 'react';
import logements from './datas/logements.json';
import './Main.css';
import { Link } from 'react-router-dom';
import Cover from './Cover';


function Cartes() {
  return (
    <div id="cntCard">
      {logements.map((logement) => (
        <Link to={`/logements/${logement.id}`} key={logement.id} className="cartes">
          <div className="filter" ></div>
          <h2>{logement.title}</h2>
          <Cover logement={logement} /> {/* Passer logement comme une prop à Cover */}
        </Link>
      ))}
    </div>
  );
}

export default Cartes;
