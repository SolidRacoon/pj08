// Navigation.js
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import bannerImage from '../assets/LOGO.svg';
import './Main.css';

function Navigation() {
    const location = useLocation();

    return (
        <nav>
            <img src={bannerImage} alt="Bannière" />
            <ul>
                <li>
                    <Link to="/" id="accueil" className={location.pathname === '/' ? 'active' : ''}>
                        Accueil
                    </Link>
                </li>
                <li>
                    <Link to="/about" id="apropos" className={location.pathname === '/about' ? 'active' : ''}>
                        À propos
                    </Link>
                </li>
            </ul>
        </nav>
    );
}

export default Navigation;
