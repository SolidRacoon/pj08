import React from 'react';
import { Link } from 'react-router-dom';
import logements from './datas/logements.json';

function Logements() {
  return (
    <div>
      <h2>Liste des logements</h2>
      <div className="logements-list">
        {logements.map((logement) => (
          <div key={logement.id} className="logement-card">
            <h3>{logement.title}</h3>
            <p>{logement.description}</p>
            <Link to={`/logements/${logement.id}`}>Voir les détails</Link>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Logements;
