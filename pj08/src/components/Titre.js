// Capsule du nom du logement
import React from 'react';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import './Main.css';

function Titre({ title }) {


  return <h3>{title}</h3>;
}

Titre.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Titre;
