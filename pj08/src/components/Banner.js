// Banner.js
import React from 'react';
import { useLocation } from 'react-router-dom';
import './Main.css';

function Banner() {
    const location = useLocation();

    return (
        <div className={`banner ${location.pathname === '/about' ? 'about-banner' : ''}`}>
            <div className="filterBanner"></div>
            <div className="banner-content">
                <h1>{location.pathname === '/about' ? '' : 'Chez vous, partout et ailleurs'}</h1>
            </div>
        </div>
    );
}

export default Banner;
