// Affichage de la page d'accueil
import React, { useEffect, useState } from 'react';
import Banner from './Banner';
import Layout from './Layout';
import Cartes from './Cartes';

function Home() {


  return (
    <Layout>
      <Banner />
      <Cartes />
    </Layout>
  );
}

export default Home;
