// Capsule de 404
import React from 'react';
import { Link } from 'react-router-dom';
import Layout from './Layout';
import './Main.css';


function NotFound() {
  return (
    <Layout>
      <div id="ttlError">
        <p id="nerror">
          404
        </p>
        <p id="error">
          Oups! La page que vous demandez n'existe pas.
        </p>
        <Link id="link" to="/">
          Retourner sur la page d’accueil
        </Link>

      </div>
    </Layout>
  );
}

export default NotFound;
