//Capsule de récupération Equipements
import React from 'react';
import { useParams } from 'react-router-dom';
import data from './datas/logements.json';

function ContEquip() {
    const { id } = useParams();
    const logement = data.find(item => item.id === id);
    return (
        <p>Equipements : {logement.equipments.join(', ')}</p>
    );
}
export default ContEquip;
