// Layout de ma page d'accueil
import React from 'react';
import Navigation from './Navigation';
import PiedDePage from './pdp';

function Layout({ children }) {
  return (
    <div>
      <header>
        <Navigation />
      </header>
      <main>
        {children}
      </main>
      <footer>
        <PiedDePage />
      </footer>
    </div>
  );
}

export default Layout;
