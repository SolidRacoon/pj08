//Affichage de la page d'une location
import React from 'react';
import Layout from './Layout';
import Titre from './Titre';
import Opener from './Opener';
import ContDescription from './recupDesc';
import Host from './Host';
import Star from './Star';
import Location from './Location';
import Tags from './Tags';
import ContEquip from './recupEquip';
import Carousel from './Carousel';
import Check from './check';
import './Main.css';
import data from './datas/logements.json';
import { useParams, Navigate } from 'react-router-dom';

function LogementDetail()
{
  const { id } = useParams();
  const logement = data.find(item => item.id === id);
  const descriptionContent = <ContDescription />;
  const equipementsContent = <ContEquip />;
  if (!logement) {
    // Gère le cas où logement n'est pas défini
    return <Navigate to="/404" />;
  }



  return (
    <Layout>
    <Check />
      <div>
        <Carousel pictures={logement.pictures} />

        <div className="topDesc">
          <div className="titreCont">
            <Titre title={logement.title} />
            <Location location={logement.location} />
            <Tags tags={logement.tags} />
          </div>
          <div className="hostCont">
            <Star rating={logement.rating} />
            <Host host={logement.host} />

          </div>
        </div>
        <div className="descCont">
          <Opener title="Description" content={descriptionContent} />
          <Opener title="Équipements" content={equipementsContent} />
        </div>
       </div>
    </Layout>
  );
}

export default LogementDetail;

