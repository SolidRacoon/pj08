//Capsule Pied de page
import './Main.css';
import React from 'react';
import Logo from '../assets/LOGOw.svg';

function PiedDePage() {
  return (
    <footer>
    <div>
    <img className="pdpsvg" src={Logo} alt="LOGO" />
      <p>© 2020 Kasa. All rights reserved</p>
    </div>
    </footer>
  );
}

export default PiedDePage;
