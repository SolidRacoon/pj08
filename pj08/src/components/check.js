// Capsule de verification
import React from 'react';
import { Navigate, useParams } from 'react-router-dom';
import data from './datas/logements.json';

function Check() {
  const { id } = useParams();
  const logement = data.find(item => item.id === id);

  if (!logement)
  {
// Redirige vers la page d'erreur si l'ID n'est pas valide
    return <Navigate to="/404" replace />;
  }
// Retourne null si le logement est trouvé
  return null;
}

export default Check;
