// Capsule pour afficher l'image de couverture d'une carte
import React from 'react';
import { useParams } from 'react-router-dom';
import './Main.css';
function Cover({ logement }) {
// Utilisez la prop logement pour afficher la couverture
  return (
    <div>
      <img className="carteImg" src={logement.cover} alt="Image de couverture" />
    </div>
  );
}

export default Cover;
