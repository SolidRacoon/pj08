//capsule de comportement collapse
import React, { useState } from 'react';
import './Main.css';
import aTop from '../assets/aTop.svg';

function Opener({ title, content }) {
  const [isCollapsed, setIsCollapsed] = useState(true);
  const toggleDescription = () => {
    setIsCollapsed(!isCollapsed);
  };

  return (
    <div className={`Desc ${isCollapsed ? 'collapsed' : ''}`}>
      <div onClick={toggleDescription} className="titreimg">
        <h2>{title}</h2>
        <img
          src={aTop}
          alt={isCollapsed ? 'Flèche vers le bas' : 'Flèche vers le haut'}
          style={{
            transform: isCollapsed ? 'rotate(180deg)' : 'rotate(0deg)',
          }}
        />
      </div>

      {!isCollapsed && <div className="ctnu">{content}</div>}
    </div>
  );
}

export default Opener;
