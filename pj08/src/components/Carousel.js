import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';
import Right from '../assets/Right.svg';
import Left from '../assets/Left.svg';
import './Main.css';

function Carousel({ pictures }) {
  const [currentImageIndex, setCurrentImageIndex] = useState(0);

  const nextImage = () => {
    const nextIndex = (currentImageIndex + 1) % pictures.length;
    setCurrentImageIndex(nextIndex);
  };

  const prevImage = () => {
    const prevIndex = (currentImageIndex - 1 + pictures.length) % pictures.length;
    setCurrentImageIndex(prevIndex);
  };

  if (!pictures || pictures.length === 0) {
    // Redirection vers la page d'erreur si les données ne sont pas valides
    return <Navigate to="/404" />;
  }

  return (
    <div id="divCar">
      <img
        className="imgCar"
        src={pictures[currentImageIndex]}
        alt={`Image ${currentImageIndex + 1}`}
      />
      {pictures.length > 1 && (
        <div className="carousel-navigation">
          <img
            src={Left}
            alt="Previous"
            className="carousel-arrow"
            onClick={prevImage}
          />
          <img
            src={Right}
            alt="Next"
            className="carousel-arrow"
            onClick={nextImage}
          />
          {window.innerWidth > 720 && (
            <div className="image-counter">
              {`${currentImageIndex + 1}/${pictures.length}`}
            </div>
          )}
        </div>
      )}
    </div>
  );
}

export default Carousel;
