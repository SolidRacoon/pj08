// Capsule des notes
import React from 'react';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import Rating1 from '../assets/1s.png';
import Rating2 from '../assets/2s.png';
import Rating3 from '../assets/3s.png';
import Rating4 from '../assets/4s.png';
import Rating5 from '../assets/5s.png';
import './Main.css';

function Star({ rating }) {

    let starImage;

    if (rating === "1") {
        starImage = Rating1;
    } else if (rating === "2") {
        starImage = Rating2;
    } else if (rating === "3") {
        starImage = Rating3;
    } else if (rating === "4") {
        starImage = Rating4;
    } else if (rating === "5") {
        starImage = Rating5;
    } else {
        starImage = null;
    }

    return (
         <div id="contStar">
             {starImage && <img className="star" src={starImage} alt={`${rating} étoile(s)`} />}
        </div>
    );
}

Star.propTypes = {
  rating: PropTypes.string.isRequired,
};

export default Star;
