// Capsule tag du logement
import React from 'react';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import './Main.css';

function Tags({ tags }) {

  return (
    <div className="tags">
      {tags.map((tag, index) => (
        <div key={index} className="tag">
          {tag}
        </div>
      ))}
    </div>
  );
}

Tags.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Tags;
