// Capsule de récupération nom et photo de l'host
import React from 'react';
import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import './Main.css';

function Host({ host }) {

    return (
        <div id="host">
            <div id="name">{host.name}</div>
            <div id="photo">
                <img src={host.picture} alt="Image de l'hôte" />
            </div>
        </div>
    );
}

Host.propTypes = {
    host: PropTypes.shape({
        name: PropTypes.string.isRequired,
        picture: PropTypes.string.isRequired,
    }).isRequired,
};

export default Host;
